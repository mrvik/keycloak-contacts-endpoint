plugins {
    alias(libs.plugins.kotlin.jvm)
}

group = "dev.mrvik"
version = "1.2-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(libs.junit.jupiter)
    testImplementation(libs.kotlin.test)
    testImplementation(libs.keycloak.server.spi)
    testImplementation(libs.keycloak.server.spi.private)
    testImplementation(libs.keycloak.core)
    testImplementation(libs.keycloak.services)
    testImplementation(libs.jersey.common)
    testImplementation(libs.kotlin.reflect)

    compileOnly(libs.keycloak.server.spi)
    compileOnly(libs.keycloak.server.spi.private)
    compileOnly(libs.keycloak.core)
    compileOnly(libs.keycloak.services)
}

kotlin {
    jvmToolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.WARN
    exclude("META-INF/*.SF", "META-INF/*.dsa", "META-INF/*.RSA", "META-INF/*.MF")
    archiveBaseName.set(rootProject.name)
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
}