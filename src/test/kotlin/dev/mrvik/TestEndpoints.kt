package dev.mrvik

import dev.mrvik.common.ResourceTemplate
import dev.mrvik.contactsEndpoint.ContactsResource
import dev.mrvik.contactsEndpoint.ContactsResourceProvider
import dev.mrvik.contactsEndpoint.ContactsResourceProviderFactory
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.OPTIONS
import jakarta.ws.rs.Path
import jakarta.ws.rs.core.Response
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.keycloak.http.HttpRequest
import org.keycloak.http.HttpResponse
import org.keycloak.models.KeycloakSession
import org.keycloak.services.DefaultKeycloakContext
import org.keycloak.services.DefaultKeycloakSession
import org.keycloak.services.DefaultKeycloakSessionFactory
import org.keycloak.services.resource.RealmResourceProvider
import org.keycloak.services.resource.RealmResourceProviderFactory
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

private fun assertClass(expected: Class<*>, got: Class<*>) {
    assert(expected.isAssignableFrom(got)) { "Bad class, got ${got.name} but ${expected.name} was expected" }
}

internal class TestEndpoints {
    companion object {
        private val sessionFactory = object : DefaultKeycloakSessionFactory() {
            override fun create(): KeycloakSession? = object : DefaultKeycloakSession(this) {
                override fun createKeycloakContext(p0: KeycloakSession?): DefaultKeycloakContext? =
                    object : DefaultKeycloakContext(p0) {
                        override fun createHttpRequest(): HttpRequest? = null
                        override fun createHttpResponse(): HttpResponse? = null
                    }
            }
        }

        private val testClasses = listOf(
            ClassTest(
                ContactsResourceProviderFactory(),
                ContactsResourceProvider::class,
                ContactsResource::class,
                "user-contacts"
            ),
        )
    }

    private data class ClassTest(
        val factory: RealmResourceProviderFactory,
        val expectedProvider: KClass<out RealmResourceProvider>,
        val expectedResource: KClass<out Any>,
        val expectedID: String,
    )

    @Test
    fun testFactory() {
        testClasses.forEach {
            val session = sessionFactory.create()!!
            assertThrows<NotAuthorizedException> { it.factory.create(null) }
            assertClass(it.expectedProvider.java, it.factory.create(session).javaClass)
            assertEquals(it.expectedID, it.factory.id)
        }
    }

    @Test
    fun testResource() {
        testClasses.forEach {
            val session = sessionFactory.create()!!
            val resourceProvider = it.factory.create(session)
            val resource = resourceProvider.resource
            assertNotNull(resource)
            assertClass(it.expectedResource.java, resource.javaClass)
            assertClass(ResourceTemplate::class.java, resource.javaClass)

            val connOptions = assertDoesNotThrow { (resource as ResourceTemplate).getConnOptions() }
            assertNotNull(connOptions)
            assertClass(Response::class.java, connOptions.javaClass)

            val members = listOfNotNull(
                resource.javaClass.kotlin.members,
                resource.javaClass.kotlin.allSuperclasses.flatMap { c -> c.members }
            ).flatten()

            assertNotNull(
                members.find { m -> m.hasAnnotation<OPTIONS>() },
                "None of the members of ${resource.javaClass.kotlin.qualifiedName} has OPTIONS"
            )

            assert(
                members.find { m -> m.hasAnnotation<Path>() } == null ||
                        (members.find { m -> m.findAnnotation<OPTIONS>() != null }!!)
                            .let { optionsMethod ->
                                val annotation = optionsMethod.findAnnotation<Path>()
                                    ?: throw Error("Method ${optionsMethod.name} is expected to have Path annotation but null was received")
                                if (!annotation.value.matches(Regex(""".+\{.+: \.\*}"""))) throw Error("Method ${optionsMethod.name} has value ${annotation.value} but a catchall was expected")
                                true
                            }
            ) { "OPTIONS element on ${resource.javaClass.kotlin.qualifiedName} has no catchall @Path but another element has @Path " }
        }
    }
}