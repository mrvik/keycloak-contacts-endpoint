package dev.mrvik.common

import jakarta.ws.rs.*
import jakarta.ws.rs.core.HttpHeaders
import jakarta.ws.rs.core.Response
import org.keycloak.models.KeycloakSession
import org.keycloak.services.managers.AppAuthManager
import org.keycloak.services.managers.AuthenticationManager

abstract class ResourceTemplate(protected val session: KeycloakSession) {
    protected fun initSession(): AuthenticationManager.AuthResult {
        val appAuthManager = AppAuthManager()
        val realm = session.context.realm

        try {
            return appAuthManager.authenticateIdentityCookie(session, realm)
                ?: bearerAuth()!! // Force an NPE if no auth present
        } catch (ex: Exception) {
            if (ex is ClientErrorException) throw ex

            throw NotAuthorizedException("Cannot authorize user with provided credentials")
        }
    }

    private fun bearerAuth(): AuthenticationManager.AuthResult? = AppAuthManager.BearerTokenAuthenticator(session).authenticate()

    @Path("/{ignored: .*}")
    @OPTIONS
    fun getConnOptions(): Response {
        val methods = listOf(HttpMethod.OPTIONS, HttpMethod.GET)
        val headers = listOf(HttpHeaders.AUTHORIZATION, HttpHeaders.ACCEPT)

        return generateResponse(
            Response.noContent()
                .header("Allow", methods.joinToString())
                .header("Access-Control-Allow-Methods", methods.joinToString(","))
                .header("Access-Control-Allow-Headers", headers.joinToString(","))
        )
    }


    protected fun generateResponse(from: Response.ResponseBuilder): Response = from
        .header("Access-Control-Allow-Origin", "*")
        .build()
}