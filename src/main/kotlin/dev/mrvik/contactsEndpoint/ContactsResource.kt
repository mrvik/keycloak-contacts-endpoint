package dev.mrvik.contactsEndpoint

import dev.mrvik.common.ResourceTemplate
import jakarta.ws.rs.*
import jakarta.ws.rs.core.Response
import org.keycloak.models.KeycloakSession
import org.keycloak.models.UserModel

class ContactsResource(session: KeycloakSession) : ResourceTemplate(session) {
    companion object {
        private val allowedAttributes by lazy {
            System.getenv("CONTACT_SEARCH_ATTRIBUTES")
                ?.split(",")?.map(String::trim)?.toSet()
                ?: setOf("zone", "area")
        }
        private const val defaultSearch = "zone"
        private const val groupSearchAttribute = "searchable"
    }

    @GET
    @Path("/by-group")
    @Produces("application/json")
    fun contactsByGroup(@QueryParam("name") groupName: String?): Response {
        val auth = initSession()
        val userService = session.users()

        return auth.user.groupsStream
            .filter { it.attributes[groupSearchAttribute]?.contains("yes") ?: false }
            .let {
                if (!groupName.isNullOrBlank()) it.filter { g -> g.name == groupName }
                else it
            }
            .flatMap {
                userService.getGroupMembersStream(session.context.realm, it)
            }
            .distinct()
            .map(::modelToContact)
            .toList()
            .let {
                if (it.isEmpty()) generateResponse(Response.noContent())
                else generateResponse(Response.ok(it))
            }
    }

    @GET
    @Path("/by-attribute")
    @Produces("application/json")
    fun getContacts(@DefaultValue(defaultSearch) @QueryParam("attribute") findAttribute: String): Response {
        if (!allowedAttributes.contains(findAttribute)) throw BadRequestException("Not allowed to match users with attribute '$findAttribute'")

        val authResult = initSession()
        val modelAttrValue = authResult.user.attributes[findAttribute]?.first()

        if (modelAttrValue.isNullOrBlank()) throw BadRequestException(
            "Current user has no \"$findAttribute\" attribute to compare"
        )

        return session.users().searchForUserByUserAttributeStream(session.context.realm, findAttribute, modelAttrValue)
            .map(::modelToContact)
            .toList()
            .let {
                if (it.isEmpty()) generateResponse(Response.noContent())
                else generateResponse(Response.ok(it))
            }
    }

    private fun modelToContact(user: UserModel): ReplyContact = ReplyContact(
        id = user.id,
        username = user.username,
        firstName = user.firstName,
        lastName = user.lastName,
    )

    private data class ReplyContact(
        val id: String,
        val username: String,
        val firstName: String?,
        val lastName: String?,
    )
}
