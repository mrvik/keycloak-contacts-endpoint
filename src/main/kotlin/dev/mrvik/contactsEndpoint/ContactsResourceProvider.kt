package dev.mrvik.contactsEndpoint

import org.keycloak.models.KeycloakSession
import org.keycloak.services.resource.RealmResourceProvider

class ContactsResourceProvider(private val session: KeycloakSession) : RealmResourceProvider {
    override fun close() {
    }

    override fun getResource(): Any {
        return ContactsResource(session)
    }
}
