package dev.mrvik.contactsEndpoint

import jakarta.ws.rs.NotAuthorizedException
import org.keycloak.Config
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.services.resource.RealmResourceProvider
import org.keycloak.services.resource.RealmResourceProviderFactory

class ContactsResourceProviderFactory : RealmResourceProviderFactory {
    override fun create(session: KeycloakSession?): RealmResourceProvider {
        if(session == null) throw NotAuthorizedException("No session was provided")
        return ContactsResourceProvider(session)
    }

    override fun init(config: Config.Scope?) {
    }

    override fun postInit(factory: KeycloakSessionFactory?) {
    }

    override fun close() {
    }

    override fun getId(): String {
        return "user-contacts"
    }
}